# How do we develop:
### Language Bases:

The most basic principles for the development 
of any project are described in the following links:

* For all programming languages must follow 
  [SOLID](https://habr.com/ru/post/508086/).
* Use [pep8](https://www.python.org/dev/peps/pep-0008/) and 
  [zen of python](https://en.wikipedia.org/wiki/Zen_of_Python).

### Work in a team:
Making project in team leads to problems and helpers, 
which can be solved by following a certain workflow. 
Some of these principles and tips are valid 
for all types of projects on **Python**.

#### What we want and how we reach it:

* Supportable project &rarr; Follow solid, 
  design project structure then code, you can use the template from
  [DL project structure and data](#Project structure and data)
* Readable code &rarr; Use typings, editing & pep8, docstrings 
  for more see [Examples](#Examples)
* Reproducible results &rarr; Follow instructions of development in
  [Reproducibility](#Reproducibility)
* Fewer bugs &rarr; See more about code review and tests in 
  [Review and Tests tips](#Review and Tests tips)
  

<br>

#DL Project:

All **DL projects** must have a flexible dataset structure, tested transforms, 
reproducible and working models. Split code in subdirectories if it implies 
more functionality more than for one model. 


#### Technical task:
 
* clear task: what is input, what is output for your system
  
* choose metrics: what is important for this task 
  (precision or recall for classification)
  
* define system conditions on deployment: 
  how many processors, gpu, memory will be on the machine, 
  how many client requests will be per day

#### Reprocudibility:

* Use Docker and fix all libs versions. Also add `.dockerignore` to skip
loading heavy data parts.

* Fix all random states or make them fixable. 

* Always use `with torch.no_grads()` or `model.eval()` 
  to make val predictions. Save model with no grad option. 

#### Project structure and data:
* Collect as much data as you can.
  
* Take a day to analyse data. Ask yourself a question is it balanced? 
  Or you need to collect more of a certain class. 
  
* Check valid splits to prevent data leaks. 
  
* Valid and clear names. **Don't name everything utils**. 

* Split code into logical parts. 
  Don't be afraid to create more dirs or files.
  It is easy to read 5 files with 200 code lines than one with 1000. 
  
* Use augmentations from `Albumentaion` or write it by yourself. 
  Don't use torch with PIL - it is slow.
  
* Create Datasets with one template with the parent of `torch.utils.data.Dataset`.

* Test augmentations manually to check if everything is correct. 
  For example, you don't apply double normalization. 
  (too small values leads to problems with training)
  
* Write preprocessing to the dataset to avoid time loss on the training step. 


#### DL Model:

* Google before choosing a model. For example, on 
  [papers with code](https://paperswithcode.com/). Here you also can find datasets. If you can use an open-source model to make a baseline and see how it works. That also allows you to check if data markup is valid (see wrong predictions of sota model).

* Always use one batch overfit to validate if the model, data or training step don't have a bug. Fix random seeds,
  get rid of augmentations and overfit model on one batch. 
  If the model is capable to remember - it is valid to learn.
  
* Validate loss and metrics. Check you get correct answers for dummy inputs 
  of handwritten losses and metrics.
  
* Init weights or use pretrained weights.

* Use schedulers to save model, to reduce lr. 

* Try to keep the model configurable. 


#### Review and Tests tips:

* Ask reviewer of time and manage yours.
* Do it in time. Don't create a blocker for another person. Don't have a blocker.
* All comments should be one of 
  * it can be done better like this
  * it seems to be a bad way to solve it, 
    can we think together or ask someone else?
    
* Use [pytest](https://habr.com/ru/post/448782/) 
  to test your post-processing code, see more in [Examples](#Examples). 
  Remember **Tested code - reliable code**. 
  
* Use github/gitlab CI/CD to create an automatic test of your model. 
Make inference examples accessible for proof of concept and easy check. 

#### Gitlab flow: 
* Every feature must be developed in a different branch
* Create a branch for feature, make `[WIP] feature_name` merge request
  and add a description. Everybody should understand 
  what feature you developing.
* Run test locally or in the cloud before and after merge for both branches. 
  Or create CI/CD test on merge request. `Bugs shall not pass!`
* Don't use `git add .` Make readable and clear commits for every file
  you changed. When you will merge it you can stash commits. 
* **Never** push forward to the master branch

<br>

# Examples:
* [PyTests](https://habr.com/ru/post/448782/)
  For example, you have some functional code as in  `src/metric.py`
  and you can check if it is correct on dummy examples as
  in `tests/test_metric.py`.
  There are a lot of possibilities to check all parts of code made 
  in functional way. You can read about more in link above. Also you can 
  run this example by command `make build` then `make tests`.

* [Typings](https://mypy.readthedocs.io/en/stable/)
  You add notation of type for all variables even complex one: 
  ````
  from typing import List, Union
  
  import numpy as np 
  from torch.utils.data import DataLoader
  
  loader_type = Dataloader[Union[List[int], np.ndarray]] 
  
  def func(dataloader: loader_type) -> None:
    ...
  ````
  It helps to understand and check what types are inputs and outputs.  


* [CodeStyle](https://pypi.org/project/flake8/) 
  
  You have one style of editing code. It can be installed in pycharm.


* [Docstrings](https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html)
  
  It helps to visually make it easier to read. 
  Not every module requires docstring, 
  don't hesitate to lead comments at a tricky function 
  to let reviewer or support to follow line by line. 