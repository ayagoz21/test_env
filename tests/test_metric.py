from src.metric import levenshteinDistanceDP
def test_levenshtein():
    assert levenshteinDistanceDP("", "") == 0
    assert levenshteinDistanceDP("kelm", "hello") == 3
    assert levenshteinDistanceDP("hello", "hello") == 0
    assert levenshteinDistanceDP("olleh", "hello") == 4
    assert levenshteinDistanceDP("ab", "hello") == 5

