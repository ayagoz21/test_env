NAME?=test_env

GPUS?=all
ifeq ($(GPUS),none)
	GPUS_OPTION=
else
	GPUS_OPTION=--gpus=$(GPUS)
endif

PROJ_ROOT?=
CODE_ROOT?=$(PROJ_ROOT)src/

.PHONY:  build stop run-dev attach exec jupyter_notebook install_checkers lint run-test run-jupyter tests

build:
	docker build \
	-t $(NAME) .

stop:
	-docker stop $(NAME)
	-docker rm $(NAME)

exec:
	docker exec -it $(NAME) bash

attach:
	docker attach $(NAME)

run-dev:
	docker run --rm -it \
		--net=host \
		--ipc=host \
		$(GPUS_OPTION) \
		-v $(shell pwd):/workdir \
		--name=$(NAME) \
		$(NAME) \
		bash

install_checkers:
	pip install mypy flake8

lint: install_checkers
	mypy $(CODE_ROOT) --config-file $(PROJ_ROOT)setup.cfg
	flake8 $(CODE_ROOT) --config $(PROJ_ROOT)setup.cfg

run-test:
	docker run --rm -it \
		--name=$(NAME) \
		$(NAME) \
		make lint


tests:
	docker run --rm -it \
		--name=$(NAME) \
		$(NAME) \
		pytest -s tests/

install_notebook:
	pip install notebook

notebook: install_notebook
	jupyter-notebook --ip 0.0.0.0 --port 8888 --allow-root

run-jupyter:
	docker run \
		--ipc=host \
		--name=$(NAME) \
		$(NAME) \
		make notebook
