FROM ghcr.io/osai-ai/dokai:21.05-pytorch
WORKDIR /workdir

RUN pip3 install librosa==0.8.0 \
    tqdm==4.45.0 \
    pytest==6.2.4

COPY . /workdir