# test_env


### Dockerfile 

Consist of basic docker image with last versions of pytorch.
You can look through repo [dokai](https://github.com/osai-ai/dokai)
and find correct versions for you or build manually 
as in [nvidia-repo](https://github.com/NVIDIA/nvidia-docker/blob/master/docker/Dockerfile.ubuntu).





### Makefile
Make file consist of command to work with docker and tests.
* `make build` - builds docker container
* `make run-dev` - runs and attaches docker container
* `make lint` - runs flake8 and mypy tests on source code (src dir)
* `make run-test` - runs and attaches docker, then runs `make lint`
* `make stop` - stops and removes container
* `make attach` - same as `docker attach`
* `make exec` - same as `docker exec`
* `make run-jupyter` - runs docker and starts jupyter notebook

`run-dev` has additional options:

* it runs container with volume. 
  That means that you local directory is mirrored by docker. 
  Any changes in docker will appear in you local directory.
* it sets ip by host. 
  It means that everything in your 
  docker network will be seen it machine network.

### Jupyter notebook

You can run manually:

```
make build
make run-dev
```
Then run in docker following command:
```
jupyter-notebook --ip 0.0.0.0 --port 8888 --allow-root & 
```
then you will see output like this:
```
[I 07:54:15.141 NotebookApp] Writing notebook server cookie secret to /root/.local/share/jupyter/runtime/notebook_cookie_secret
[I 07:54:15.313 NotebookApp] Serving notebooks from local directory: /workdir
[I 07:54:15.313 NotebookApp] Jupyter Notebook 6.3.0 is running at:
[I 07:54:15.313 NotebookApp] http://42870:8888/?token=5fbd21e23fa924b405e9627052bbf0c1be5484ec63733d65
[I 07:54:15.314 NotebookApp]  or http://127.0.0.1:8888/?token=5fbd21e23fa924b405e9627052bbf0c1be5484ec63733d65
[I 07:54:15.314 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
[W 07:54:15.316 NotebookApp] No web browser found: could not locate runnable browser.
```

then you can attach your notebook by command on you local machine:

`ssh -L 127.0.0.1:8888:localhost:8888 -N root@xx.xx.xx.xx`


Or you can set up make file command 
which will handle all of the commands above: 
```
make build
make run-jupyter
```

There are a problem to stop docker image 
you need one more terminal and run `make-stop`. 
See `tmux` app, can help. 